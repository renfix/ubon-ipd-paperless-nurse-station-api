import { Knex } from 'knex';

export class VitalSignModel {

    // Create by RENFIX
    insertVitalSign(db: Knex, data: any) {
        return db('vital_sign')
            .insert(data)
            .onConflict(['admit_id','vital_sign_date','vital_sign_time'])
            .merge()
            .returning('*');
    }

    // get Virtual Sign + admit
    getVitalSignByTableAdmit(db: Knex, admit_id: any) {
        return db('admit')
            .where('admit.id', admit_id);
    }

    getVitalSignByTableVital_Sign(db: Knex, admit_id: any) {
        return db('vital_sign')
            .where('vital_sign.admit_id', admit_id);
    }
}