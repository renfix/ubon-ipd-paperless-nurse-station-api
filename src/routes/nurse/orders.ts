import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { UUID } from 'crypto';
import { OdersModel } from '../../models/nurse/oders';


export default async (fastify: FastifyInstance, _options: any, done: any) => {


    const db: Knex = fastify.db;
    const oserServices = new OdersModel();

    fastify.put('/:order_id', {
        preHandler: [
            fastify.guard.role('nurse', 'admin', 'doctor'),
            // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
        ],
        //schema: updateSchema,
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        try {

            const params: any = request.params;
            const { order_id } = params;
            
            const req: any = request
            const data: any = req.body

            await oserServices.updateOrders(db, order_id, data);
            return reply.status(StatusCodes.OK)
            .send({ status: 'ok' });
        } catch (error: any) {
            request.log.info(error.message);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
        }
    })

}