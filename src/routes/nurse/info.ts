import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

import { InfoModel } from '../../models/nurse/info';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const infoModel = new InfoModel()

  fastify.get('/:id', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.params
      const id: any = req.id

      const data: any = await infoModel.info(db,id)
      console.log({data})

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data,
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

}