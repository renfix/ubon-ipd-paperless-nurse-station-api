import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { PatientModel } from '../../models/patient/patient';
import anSchema from '../../schema/nurse/patient';


export default async (fastify: FastifyInstance, _options: any, done: any) => {
  const db: Knex = fastify.db;
  const patientModel = new PatientModel()

  fastify.post('/', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      // jwt decoded
      const userId: any = request.user.sub;
      const query: any = request.query
      const id = query.id
      const body: any = request.body;

      const data: any = { ...body, create_by: userId}

      await patientModel.save(db, data);

      return reply.status(StatusCodes.CREATED).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  // get patient
  fastify.get('/:an', {
    preHandler: [
      fastify.guard.role('nurse', 'admin', 'doctor'),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: anSchema
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const {an} = params;

      const data = await patientModel.infoByAN(db, an);

      return reply.status(StatusCodes.CREATED).send({ ok: true,data });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

}